==================================================================
https://keybase.io/jacobmassey
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://jacobmassey.com
  * I am jacobmassey (https://keybase.io/jacobmassey) on keybase.
  * I have a public key ASC5c4-uREaVVkSPLFy6Keg9hGjhAkXd-HHQK2Ji6uF7mQo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01207beef70716fe9d3d8deab273d6078da9f75cb135bd84a090bcbdc286002907860a",
      "host": "keybase.io",
      "kid": "0120b9738fae44469556448f2c5cba29e83d8468e10245ddf871d02b6262eae17b990a",
      "uid": "6149b29ae3efff40d9e7298581786e19",
      "username": "jacobmassey"
    },
    "merkle_root": {
      "ctime": 1503859433,
      "hash": "cccc703b6ff2475864dcb27406ac15325a8ef3dd49a07cf34b5088403076d76a34a54d23f2f601f843ea5313035618373cb1c16cf1c96ae6af3fd9fd07531dc2",
      "hash_meta": "1b58046ba86cb0fb18b1875b6e1b2c2c34b1e9d6143431666bb3745d04225902",
      "seqno": 1351639
    },
    "service": {
      "hostname": "jacobmassey.com",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.28"
  },
  "ctime": 1503859713,
  "expire_in": 504576000,
  "prev": "fab2d1471596fa13d524140f8fbb23b40910ac7930f09756a3fab3c744c83dae",
  "seqno": 13,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEguXOPrkRGlVZEjyxcuinoPYRo4QJF3fhx0CtiYurhe5kKp3BheWxvYWTFA0t7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTIwN2JlZWY3MDcxNmZlOWQzZDhkZWFiMjczZDYwNzhkYTlmNzVjYjEzNWJkODRhMDkwYmNiZGMyODYwMDI5MDc4NjBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwYjk3MzhmYWU0NDQ2OTU1NjQ0OGYyYzVjYmEyOWU4M2Q4NDY4ZTEwMjQ1ZGRmODcxZDAyYjYyNjJlYWUxN2I5OTBhIiwidWlkIjoiNjE0OWIyOWFlM2VmZmY0MGQ5ZTcyOTg1ODE3ODZlMTkiLCJ1c2VybmFtZSI6ImphY29ibWFzc2V5In0sIm1lcmtsZV9yb290Ijp7ImN0aW1lIjoxNTAzODU5NDMzLCJoYXNoIjoiY2NjYzcwM2I2ZmYyNDc1ODY0ZGNiMjc0MDZhYzE1MzI1YThlZjNkZDQ5YTA3Y2YzNGI1MDg4NDAzMDc2ZDc2YTM0YTU0ZDIzZjJmNjAxZjg0M2VhNTMxMzAzNTYxODM3M2NiMWMxNmNmMWM5NmFlNmFmM2ZkOWZkMDc1MzFkYzIiLCJoYXNoX21ldGEiOiIxYjU4MDQ2YmE4NmNiMGZiMThiMTg3NWI2ZTFiMmMyYzM0YjFlOWQ2MTQzNDMxNjY2YmIzNzQ1ZDA0MjI1OTAyIiwic2Vxbm8iOjEzNTE2Mzl9LCJzZXJ2aWNlIjp7Imhvc3RuYW1lIjoiamFjb2JtYXNzZXkuY29tIiwicHJvdG9jb2wiOiJodHRwczoifSwidHlwZSI6IndlYl9zZXJ2aWNlX2JpbmRpbmciLCJ2ZXJzaW9uIjoxfSwiY2xpZW50Ijp7Im5hbWUiOiJrZXliYXNlLmlvIGdvIGNsaWVudCIsInZlcnNpb24iOiIxLjAuMjgifSwiY3RpbWUiOjE1MDM4NTk3MTMsImV4cGlyZV9pbiI6NTA0NTc2MDAwLCJwcmV2IjoiZmFiMmQxNDcxNTk2ZmExM2Q1MjQxNDBmOGZiYjIzYjQwOTEwYWM3OTMwZjA5NzU2YTNmYWIzYzc0NGM4M2RhZSIsInNlcW5vIjoxMywidGFnIjoic2lnbmF0dXJlIn2jc2lnxEDu2EJpVCaTTIT8Cn3AyS9fjx+pPDlWG5OFrXgvzOxv2b1l4inAIRHDYOJUo3E8a7WhQsqAZwBpImBv/Zh//lgBqHNpZ190eXBlIKRoYXNogqR0eXBlCKV2YWx1ZcQgu5pXE7RVWJMIbqe22Uu4sHGWL4TOsiZHBATHXeybEESjdGFnzQICp3ZlcnNpb24B

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/jacobmassey

==================================================================